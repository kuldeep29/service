package main

import (
	"fmt"

	"gitlab.com/kuldeep29/service/pkg/logger"
	"gitlab.com/kuldeep29/service/pkg/rest"
	"gitlab.com/kuldeep29/service/pkg/storage"
	"gitlab.com/kuldeep29/service/pkg/todo"
)

func main() {
	slog := logger.InitSugarLogger()
	defer func() {
		_ = slog.Sync() // Flushes buffer, if any
	}()
	slog.Info("Starting todo as rest api service")
	slog.Info("Initializing application for connecting to database and setting up API routes.")

	// Initliazing components needed for running web application.
	dbConfig := storage.NewMongoDBClient()
	dbClient := dbConfig.GetClientConfig()
	err := dbClient.Connect()
	if err != nil {
		panic(fmt.Errorf("failed to connect to database client: %s", err))
	}
	todoDatabase := storage.NewDatabase(dbClient)
	todoService := todo.NewTodoService(todoDatabase)
	app := rest.NewAppContext(todoService)

	slog.Info("Listening to new requests...")
	app.Run(":80")
}
