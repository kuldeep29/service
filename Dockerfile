# Dockerfile to be used in CI/CD

# Stage 1
# Setting official golang image tag 1.17 as baseimage 
FROM golang:1.17 AS buildenv
ENV GOOS=linux GOARCH=amd64 CGO_ENABLED=0
ADD . /go/src/$REPO_PATH
WORKDIR /go/src/$REPO_PATH
RUN go build -o bin/todo cmd/main.go

#################################################################

# Stage 2
# Setting official alpine linux image tag latest as baseimage 
FROM alpine:latest
RUN apk update \
  && apk add --no-cache --purge ca-certificates tzdata\
  && rm -rf /var/cache/apk/* /tmp/*
COPY --from=buildenv /go/src/$REPO_PATH/bin/todo /usr/local/bin/todo
CMD ["todo"]