#!/bin/bash

mockery --recursive=true --case=underscore --output=pkg/internal/mocks --name=DatabaseHelper --filename=mock_db_helper.go --dir=pkg/storage
mockery --recursive=true --case=underscore --output=pkg/internal/mocks --name=CollectionHelper --filename=mock_db_coll_helper.go --dir=pkg/storage
mockery --recursive=true --case=underscore --output=pkg/internal/mocks --name=CursorHelper --filename=mock_db_cur_helper.go --dir=pkg/storage
mockery --recursive=true --case=underscore --output=pkg/internal/mocks --name=ClientHelper --filename=mock_db_cl_helper.go --dir=pkg/storage
mockery --recursive=true --case=underscore --output=pkg/internal/mocks --name=Servicer --structname=MockTodoService --filename=mock_todo_service.go --dir=pkg/todo
