#!/bin/bash

cmd=$1
shift
case $cmd in
"bash")
    bash -c $@
    ;;
"test")
    CompileDaemon -build="go version" -command="go test -coverprofile=cover.out ./..."
    ;;
"build" | *)
    CompileDaemon -build="go build -o bin/todo cmd/main.go" -command="bin/todo"
    ;;
esac
