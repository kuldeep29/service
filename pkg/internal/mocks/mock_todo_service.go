// Code generated by mockery v2.9.4. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	primitive "go.mongodb.org/mongo-driver/bson/primitive"

	storage "gitlab.com/kuldeep29/service/pkg/storage"
)

// MockTodoService is an autogenerated mock type for the Servicer type
type MockTodoService struct {
	mock.Mock
}

// CreateTodo provides a mock function with given fields: _a0, _a1
func (_m *MockTodoService) CreateTodo(_a0 context.Context, _a1 *storage.Todo) error {
	ret := _m.Called(_a0, _a1)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *storage.Todo) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetTodos provides a mock function with given fields: _a0, _a1
func (_m *MockTodoService) GetTodos(_a0 context.Context, _a1 primitive.M) ([]*storage.Todo, error) {
	ret := _m.Called(_a0, _a1)

	var r0 []*storage.Todo
	if rf, ok := ret.Get(0).(func(context.Context, primitive.M) []*storage.Todo); ok {
		r0 = rf(_a0, _a1)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*storage.Todo)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, primitive.M) error); ok {
		r1 = rf(_a0, _a1)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
