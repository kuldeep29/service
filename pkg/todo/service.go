package todo

import (
	"context"
	"time"

	"gitlab.com/kuldeep29/service/pkg/storage"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Servicer provides method for the business logic
type Servicer interface {
	CreateTodo(context.Context, *storage.Todo) error
	GetTodos(context.Context, primitive.M) ([]*storage.Todo, error)
}

// NewTodoService is a factory func to create a new todo.Service.
func NewTodoService(dbHelper storage.DatabaseHelper) *Service {
	var s Service
	s.repo = storage.NewTodoRepository(dbHelper)

	return &s
}

// Service implements the Servicer methods.
type Service struct {
	repo storage.TodoRepository
}

func (s *Service) CreateTodo(context context.Context, todo *storage.Todo) error {
	now := time.Now()
	todo.Created = &now
	todo.Updated = &now
	todo.ID = primitive.NewObjectID().Hex()

	return s.repo.CreateTodo(context, todo)
}

// GetTodos return todos from database.
func (s *Service) GetTodos(context context.Context, filter primitive.M) ([]*storage.Todo, error) {

	return s.repo.GetTodos(context, filter)
}
