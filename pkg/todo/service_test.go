package todo_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/kuldeep29/service/pkg/internal/mocks"
	"gitlab.com/kuldeep29/service/pkg/storage"
	"gitlab.com/kuldeep29/service/pkg/todo"
)

// TestCreateTodo test tries creating a todo in mongodb
func TestCreateTodo(t *testing.T) {

	// Create new repository with mocked Database interface
	todoColl := &storage.Todo{Item: "mock-todo-item"}

	dbHelper := &mocks.DatabaseHelper{}
	collHelper := &mocks.CollectionHelper{}
	dbHelper.On("Collection", "todos").Return(collHelper)
	collHelper.On("InsertOne", context.TODO(), mock.AnythingOfType("*storage.Todo")).Return(nil, nil)
	todoService := todo.NewTodoService(dbHelper)
	err := todoService.CreateTodo(context.TODO(), todoColl)

	assert.NotEmpty(t, todoColl.ID)
	assert.Equal(t, "mock-todo-item", todoColl.Item)
	assert.NoError(t, err, nil)
}
