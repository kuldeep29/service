package rest

import (
	"context"
	"log"
	"net/http"
	"os"

	"github.com/google/uuid"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/kuldeep29/service/pkg/logger"
)

// ContextKey is a custom type for string
type ContextKey string

// ContextParamsKey is a constant
const (
	ContextParamsKey ContextKey = "params"
)

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS")
			w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")
			w.Header().Set("Content-Type", "application/json")
			return
		}

		next.ServeHTTP(w, r)
		log.Println("Executing middleware again")
	})
}

// WrapHandler wraps handlers
func WrapHandler(h http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		ctx := r.Context()
		ctx = context.WithValue(ctx, ContextParamsKey, ps)
		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)
	}
}

// RecoverHandler recovers the application in case of a panic
func RecoverHandler(next http.Handler) http.Handler {
	slog := logger.InitSugarLogger()
	defer func() {
		_ = slog.Sync() // Flushes buffer, if any
	}()

	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				slog.Panicw("Panic ",
					// Structured context as loosely typed key-value pairs.
					"error", err,
				)
				http.Error(w, http.StatusText(500), 500)
			}
		}()

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// LoggingHandler logs basic information about request
func LoggingHandler(next http.Handler) http.Handler {
	slog := logger.InitSugarLogger()
	defer func() {
		_ = slog.Sync() // Flushes buffer, if any
	}()

	fn := func(w http.ResponseWriter, r *http.Request) {
		slog.Infow("Request details",
			// Structured context as loosely typed key-value pairs.
			"requestID", r.Header.Get("X-Request-ID"),
			"userID", r.Header.Get("X-User-ID"),
			"method", r.Method,
			"url", r.URL.String(),
		)
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// IdentifierHandler adds a unique id in header for each request if not present
func IdentifierHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		if len(r.Header.Get("X-Request-ID")) == 0 {
			r.Header.Set("X-Request-ID", uuid.New().String())
		}

		// Add response headers.
		w.Header().Add("X-Request-ID", r.Header.Get("X-Request-ID"))
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// AuthHandler verifies authorization headers and values
func AuthHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		basicAuthHandler(w, r, next)
	}
	return http.HandlerFunc(fn)
}

// basicAuthHandler verifies basic auth credentials
func basicAuthHandler(w http.ResponseWriter, r *http.Request, next http.Handler) {
	user := os.Getenv("BASIC_AUTH_USERNAME")
	pass := os.Getenv("BASIC_AUTH_PASSWORD")

	// Check if basic auth matches our credentials
	if reqUser, reqPass, ok := r.BasicAuth(); ok {
		if reqUser == user && reqPass == pass {
			// Delegate request to the given handle
			next.ServeHTTP(w, r)
			return
		}
	}

	// Request Basic Authentication otherwise
	w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
	http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
}
