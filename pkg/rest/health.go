package rest

import (
	"encoding/json"
	"net/http"

	"gitlab.com/kuldeep29/service/pkg/rest/message"
)

// health is handler for checking health page.
func (app *AppContext) health(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("X-Request-ID", r.Header.Get("X-Request-ID"))
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(message.HealthResponse{
		Ok: true,
	})
}
