package rest

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/pact-foundation/pact-go/utils"
	"github.com/stretchr/testify/mock"
	"gitlab.com/kuldeep29/service/pkg/internal/mocks"
	"gitlab.com/kuldeep29/service/pkg/logger"
	"gitlab.com/kuldeep29/service/pkg/rest/message"
	"gitlab.com/kuldeep29/service/pkg/storage"
	"gitlab.com/kuldeep29/service/pkg/todo"
	"gopkg.in/go-playground/assert.v1"
)

// TestCreateTodo test tries creating a todo & expects status code be 201
func TestCreateTodoHandler(t *testing.T) {
	w := httptest.NewRecorder()
	todoData := message.TodoReq{
		Data: message.TodoData{
			RequestMetaData: message.RequestMetaData{
				Type: "todos",
			},
			Attributes: &storage.Todo{
				Item: "task testing create todo",
			},
		},
	}
	jsonTodo, _ := json.Marshal(todoData)
	r := httptest.NewRequest("POST", "/v1/todos", strings.NewReader(string(jsonTodo)))
	mockTodoService := &mocks.MockTodoService{}
	mockTodoService.On("CreateTodo", context.Background(), mock.AnythingOfType("*storage.Todo")).Return(nil)
	app := NewAppContext(mockTodoService)
	// Invoke code under test
	app.createTodoHandler(w, r)
	// End invocation
	assert.Equal(t, 201, w.Result().StatusCode)
}

// The Provider verification
func TestVerifyPactContract(t *testing.T) {
	go startProvider()

	pact := createPact()

	// Verify the Provider - Tag-based Published Pacts for any known consumers
	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            fmt.Sprintf("http://localhost:%d", port),
		BrokerURL:                  "https://kpal.pactflow.io",
		Provider:                   "Todo Provider",
		BrokerToken:                "0Hi8L8irii34o7FYJ4j60w",
		PublishVerificationResults: true,
		ProviderVersion:            "1.0.0",
		RequestFilter:              fixBearerToken,
	})

	if err != nil {
		t.Fatal(err)
	}
}

// Simulates the neeed to set a time-bound authorization token,
// such as an OAuth bearer token
func fixBearerToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.SetBasicAuth("admin", "123")
		next.ServeHTTP(w, r)
	})
}

// Starts the provider API with hooks for provider states.
// This essentially mirrors the main.go file, with extra routes added.
func startProvider() {
	slog := logger.InitSugarLogger()
	defer func() {
		_ = slog.Sync() // Flushes buffer, if any
	}()
	slog.Info("Starting todo as rest api service")
	slog.Info("Initializing application for connecting to database and setting up API routes.")

	// Initliazing components needed for running web application.
	dbConfig := storage.NewMongoDBClient()
	dbClient := dbConfig.GetClientConfig()
	err := dbClient.Connect()
	if err != nil {
		panic(fmt.Errorf("failed to connect to database client: %s", err))
	}
	todoDatabase := storage.NewDatabase(dbClient)
	todoService := todo.NewTodoService(todoDatabase)
	app := NewAppContext(todoService)

	slog.Info("Listening to new requests...")
	app.Run(fmt.Sprintf(":%d", port))
}

// Configuration / Test Data
var dir, _ = os.Getwd()
var pactDir = fmt.Sprintf("%s/../../pacts", dir)
var logDir = fmt.Sprintf("%s/log", dir)
var port, _ = utils.GetFreePort()

// Setup the Pact client.
func createPact() dsl.Pact {
	return dsl.Pact{
		Provider: "Todo Provider",
		LogDir:   logDir,
		PactDir:  pactDir,
		LogLevel: "DEBUG",
	}
}
