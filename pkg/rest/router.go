package rest

import (
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
)

// SetRoutes sets up routes
func (app *AppContext) SetRoutes() {
	commonHandlers := alice.New(context.ClearHandler, IdentifierHandler, LoggingHandler, RecoverHandler)
	authHandler := commonHandlers.Append(AuthHandler)

	r := mux.NewRouter()

	r.Handle("/v1/todos", authHandler.ThenFunc(app.createTodoHandler)).Methods("POST")
	r.Handle("/v1/todos", authHandler.ThenFunc(app.viewTodosHandler)).Methods("GET")
	r.Handle("/health", commonHandlers.ThenFunc(app.health)).Methods("GET")

	app.router = corsMiddleware(r)
}
