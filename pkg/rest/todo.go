package rest

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/kuldeep29/service/pkg/logger"
	"gitlab.com/kuldeep29/service/pkg/rest/message"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	todoRequest = "todos"
)

// createTodoHandler is handler for creating todo items.
func (app *AppContext) createTodoHandler(w http.ResponseWriter, r *http.Request) {
	slog := logger.InitSugarLogger()
	defer func() {
		_ = slog.Sync() // Flushes buffer, if any
	}()

	defer func() {
		_ = r.Body.Close()
	}()

	var request message.TodoReq
	var errRes message.Errors

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		slog.Errorw("Unable to read request",
			"error", err.Error(),
			"requestID", r.Header.Get("X-Request-ID"))

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(reqBody, &request)

	if err != nil {
		slog.Errorw("Unable to unmarshal request",
			"error", err.Error(),
			"requestID", r.Header.Get("X-Request-ID"))

		http.Error(w, "Unable to unmarshal request", http.StatusInternalServerError)
		return
	}

	err = app.todoService.CreateTodo(r.Context(), request.Data.Attributes)
	if err != nil {
		slog.Errorw("Invalid request",
			"error", err,
			"requestID", r.Header.Get("X-Request-ID"),
		)
		errRes.Errors = append(errRes.Errors, message.Error{
			Status: strconv.Itoa(http.StatusInternalServerError),
			Title:  "Server error",
			Source: "Service Layer",
			Detail: err.Error(),
		})

		w.WriteHeader(http.StatusInternalServerError)
		_ = json.NewEncoder(w).Encode(errRes)

		return
	}

	request.Data.RequestMetaData.ID = request.Data.Attributes.ID

	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(request)
}

// viewTodosHandler is handler for viewling list of todos.
func (app *AppContext) viewTodosHandler(w http.ResponseWriter, r *http.Request) {
	slog := logger.InitSugarLogger()
	defer func() {
		_ = slog.Sync() // Flushes buffer, if any
	}()

	defer func() {
		_ = r.Body.Close()
	}()

	var errRes message.Errors

	var response message.TodoCollection
	var count int
	data := []message.TodoData{}
	filter := primitive.M{}
	todos, err := app.todoService.GetTodos(r.Context(), filter)
	if err != nil {
		slog.Errorw("Unable to fetch todos",
			"error", err.Error(),
			"requestID", r.Header.Get("X-Request-ID"),
		)
		errRes.Errors = append(errRes.Errors, message.Error{
			Status: strconv.Itoa(http.StatusNotFound),
			Title:  "Unable to fetch todos",
			Detail: err.Error(),
		})
		w.WriteHeader(http.StatusNotFound)
		_ = json.NewEncoder(w).Encode(errRes)
		return
	}

	for _, todo := range todos {
		if todo == nil {
			continue
		}
		count++
		tData := message.TodoData{}
		tData.Attributes = todo
		tData.Type = todoRequest
		tData.ID = todo.ID

		data = append(data, tData)
	}

	response.Data = data

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(response)
}
