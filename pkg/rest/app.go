package rest

import (
	"log"
	"net/http"

	"gitlab.com/kuldeep29/service/pkg/todo"
)

// AppContext holds core components for running this web application i.e. router and todo service.
type AppContext struct {
	router      http.Handler
	todoService todo.Servicer
}

// NewAppContext is a factory func to create a new AppContext.
func NewAppContext(ts todo.Servicer) *AppContext {
	app := &AppContext{todoService: ts}
	app.SetRoutes()
	return app
}

// Run is used to run the web application
func (app *AppContext) Run(port string) {

	log.Fatal(http.ListenAndServe(port, app.router))
}
