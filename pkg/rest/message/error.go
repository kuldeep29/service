package message

// Errors represents error in response
type Errors struct {
	Errors []Error `json:"errors"`
}

// Error represents error in response
type Error struct {
	Status string `json:"status"` // Status returned in response
	Source string `json:"source"`
	Title  string `json:"title"`  // Title for error
	Detail string `json:"detail"` // Short description for error
}
