package message

import "gitlab.com/kuldeep29/service/pkg/storage"

// TodoReq represents invoice request data.
type TodoReq struct {
	Data TodoData `json:"data" validate:"required"`
}

// InvoiceData represents invoice request data.
type TodoData struct {
	RequestMetaData
	Attributes *storage.Todo `json:"attributes" validate:"required"`
}

// TodoCollection represents todo collection api.
type TodoCollection struct {
	Data []TodoData `json:"data"`
}

// RequestMetaData contains the meta information required by all request objects
type RequestMetaData struct {
	ID   string `json:"id,omitempty"`
	Type string `json:"type" validate:"required"`
}

type HealthResponse struct {
	Ok bool `json:"ok"`
}
