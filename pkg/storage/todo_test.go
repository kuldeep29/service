package storage_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/kuldeep29/service/pkg/internal/mocks"
	"gitlab.com/kuldeep29/service/pkg/storage"
)

// TestCreateTodo test tries creating a todo in mongodb
func TestCreateTodo(t *testing.T) {

	dbHelper := &mocks.DatabaseHelper{}
	collectionHelper := &mocks.CollectionHelper{}

	dbHelper.On("Collection", "todos").Return(collectionHelper)
	collectionHelper.
		On("InsertOne", context.Background(), mock.AnythingOfType("*storage.Todo")).
		Return(nil, nil)

	// Create new repository with mocked Database interface
	todoRepo := storage.NewTodoRepository(dbHelper)

	err := todoRepo.CreateTodo(context.Background(), &storage.Todo{Item: "mocked-todo-item"})

	assert.NoError(t, err, nil)
}

func TestGetTodos(t *testing.T) {

	dbHelper := &mocks.DatabaseHelper{}
	collectionHelper := &mocks.CollectionHelper{}
	crHelperCorrect := &mocks.CursorHelper{}
	crHelperErr := &mocks.CursorHelper{}

	// Because interfaces does not implement mock.Mock functions we need to use
	// type assertion to mock implemented methods
	dbHelper.On("Collection", "todos").Return(collectionHelper)
	collectionHelper.On("Find", context.Background(), nil).Return(crHelperCorrect, nil)
	crHelperCorrect.On("Next", context.TODO()).Return(true).Once()
	crHelperCorrect.On("Next", context.TODO()).Return(false)
	crHelperCorrect.On("Close", context.TODO()).Return(nil)
	crHelperCorrect.
		On("Decode", mock.AnythingOfType("*storage.Todo")).
		Return(nil).Run(func(args mock.Arguments) {
		arg := args.Get(0).(*storage.Todo)
		arg.Item = "mocked-todo-item"
	})

	// Create new repository with mocked Database interface
	todoRepo := storage.NewTodoRepository(dbHelper)

	// Call method with defined filter, that in our mocked function returns
	// mocked-error
	todos, err := todoRepo.GetTodos(context.Background(), nil)

	assert.NotEmpty(t, todos)
	assert.Equal(t, 1, len(todos))
	assert.NoError(t, err, nil)

	// Test no result found scanrio
	crHelperErr.On("Decode", mock.AnythingOfType("*storage.Todo")).Return(errors.New("mocked-error"))
	crHelperErr.On("Next", context.TODO()).Return(false)
	crHelperErr.On("Close", context.TODO()).Return(nil)

	todos, err = todoRepo.GetTodos(context.Background(), nil)
	assert.Empty(t, todos)
	assert.Equal(t, 0, len(todos))
	assert.NoError(t, err, nil)
}
