package storage

import (
	"gitlab.com/kuldeep29/service/pkg/logger"
)

// Mongo implements db configuration behaviour.
type Mongo struct {
	config ClientHelper
}

// Configer repersents configuration of db.
type Configer interface {
	SetClientConfig()
	GetClientConfig() ClientHelper
}

// NewMongoDBClient is a factory func to create a new todo.Service.
func NewMongoDBClient() *Mongo {
	var m Mongo
	m.SetClientConfig()

	return &m
}

// SetClientConfig returns mongo client
func (m *Mongo) SetClientConfig() {
	slog := logger.InitSugarLogger()
	defer func() {
		_ = slog.Sync() // Flushes buffer, if any
	}()
	client, err := NewClient()
	if err != nil {
		slog.Fatalw("Unable to get database client",
			"error", err,
		)
	}

	m.config = client
}

// GetClientConfig returns mongo client
func (m *Mongo) GetClientConfig() ClientHelper {
	return m.config
}
