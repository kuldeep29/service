package storage

import (
	"time"
)

// BaseDocument provides the data required by all collections
type BaseDocument struct {
	ID      string     `bson:"_id" json:"-"`
	Created *time.Time `bson:"created" json:"created,omitempty"`
	Updated *time.Time `bson:"updated" json:"updated,omitempty"`
}

// Todo contains todo related info.
type Todo struct {
	BaseDocument `bson:",inline"`
	Item         string `bson:"item" json:"item"`
}
