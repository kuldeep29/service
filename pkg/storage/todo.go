package storage

import (
	"context"
)

var (
	todoCollection = "todos"
)

// TodoRepository defines a set behaviours allowed in a todo.
type TodoRepository interface {
	GetTodos(context.Context, interface{}) ([]*Todo, error)
	CreateTodo(context.Context, *Todo) error
}

// todoRepo implements TodoRepoistory interface and int contact database helper.
type todoRepo struct {
	db DatabaseHelper
}

func NewTodoRepository(db DatabaseHelper) TodoRepository {
	return &todoRepo{
		db: db,
	}
}

func (tr *todoRepo) CreateTodo(ctx context.Context, todo *Todo) error {
	_, err := tr.db.Collection(todoCollection).InsertOne(ctx, todo)

	return err
}

func (tr *todoRepo) GetTodos(ctx context.Context, filter interface{}) ([]*Todo, error) {
	var todos []*Todo

	cur, err := tr.db.Collection(todoCollection).Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	defer cur.Close(context.TODO())
	for cur.Next(context.TODO()) {
		var todo Todo
		err = cur.Decode(&todo)
		if err == nil {
			todos = append(todos, &todo)
		}
	}

	return todos, err
}
