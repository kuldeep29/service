package storage

import (
	"context"
	"fmt"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// DatabaseHelper defines rules for wrapping up the mongo database methods.
type DatabaseHelper interface {
	Collection(name string) CollectionHelper
	Client() ClientHelper
}

// CollectionHelper defines rules for wrapping up the mongo collection methods.
type CollectionHelper interface {
	Find(context.Context, interface{}) (CursorHelper, error)
	InsertOne(context.Context, interface{}) (interface{}, error)
}

// CursorHelper defines rules for wrapping up update mongo cursor methods.
type CursorHelper interface {
	Decode(v interface{}) error
	Close(context.Context) error
	Next(context.Context) bool
}

// ClientHelper defines rules for wrapping up mongo client methods.
type ClientHelper interface {
	Database(string) DatabaseHelper
	Connect() error
	StartSession() (mongo.Session, error)
}

// mongoClient is a wrapper for Mongo client which is implementing ClientHelper interface.
type mongoClient struct {
	cl *mongo.Client
}

// mongoDatabase is a wrapper for Mongo database which is implementing DatabaseHelper interface.
type mongoDatabase struct {
	db *mongo.Database
}

// mongoCollection is a wrapper for Mongo Collection which is implementing CollectioinHelper interface.
type mongoCollection struct {
	coll *mongo.Collection
}

// mongoCursor is a wrapper for Mongo Cursor which is implementing CursorHelper interface.
type mongoCursor struct {
	cr *mongo.Cursor
}

// mongoSession is a wrapper for Mongo Session used in StartSession method of ClientHelper interface.
type mongoSession struct {
	mongo.Session
}

// NewClient return Mongo Client (mongoClient), since mongoClient implements ClientHelper interface.
func NewClient() (ClientHelper, error) {

	uri := os.Getenv("DB_URI")
	fmt.Println(uri)
	clientOptions := options.Client().ApplyURI(uri)
	c, err := mongo.NewClient(clientOptions)

	return &mongoClient{cl: c}, err
}

// NewDatabase returns Mongo Client (mongoClient)'s database.
func NewDatabase(client ClientHelper) DatabaseHelper {
	return client.Database(os.Getenv("DB_NAME"))
}

func (mc *mongoClient) Database(dbName string) DatabaseHelper {
	db := mc.cl.Database(dbName)
	return &mongoDatabase{db: db}
}

func (mc *mongoClient) StartSession() (mongo.Session, error) {
	session, err := mc.cl.StartSession()
	return &mongoSession{session}, err
}

func (mc *mongoClient) Connect() error {
	return mc.cl.Connect(context.TODO())
}

func (md *mongoDatabase) Collection(colName string) CollectionHelper {
	collection := md.db.Collection(colName)
	return &mongoCollection{coll: collection}
}

func (md *mongoDatabase) Client() ClientHelper {
	client := md.db.Client()
	return &mongoClient{cl: client}
}

func (mc *mongoCollection) Find(ctx context.Context, filter interface{}) (CursorHelper, error) {
	cursor, err := mc.coll.Find(ctx, filter)
	return &mongoCursor{cr: cursor}, err
}

func (mc *mongoCollection) InsertOne(ctx context.Context, document interface{}) (interface{}, error) {
	id, err := mc.coll.InsertOne(ctx, document)
	return id.InsertedID, err
}

func (mc *mongoCollection) DeleteOne(ctx context.Context, filter interface{}) (int64, error) {
	count, err := mc.coll.DeleteOne(ctx, filter)
	return count.DeletedCount, err
}

func (mc *mongoCursor) Decode(v interface{}) error {
	return mc.cr.Decode(v)
}

func (mc *mongoCursor) Close(ct context.Context) error {
	return mc.cr.Close(ct)
}

func (mc *mongoCursor) Next(ct context.Context) bool {
	return mc.cr.Next(ct)
}
