# rest
This is backend service based on GOLang for storing TODO's info.

## Local Setup

Switch to the directory service, create local version of .env file by copying from .sample.env and run:

```
cp .sample.env .env
docker-compose up
```

This should bring up 1 containers of this golang application.

## Test

To run test cases, please get into the container by 
```
docker exec -it todo-service bash 
```
and then inside the container please run below script

```
./scripts/test.sh
```


## Production build and deploy.

* To store docker images I am using google `[Artifact Registry](https://cloud.google.com/artifact-registry)`. To build and push the docker please run below script.

```
./scripts/build.sh
```

* To create deployment in kubernets and create ingress resource, please run below script.

```
./scripts/deploy.sh
```

**Important Note**

Currently I am using my google account to manage google kubernetes cluster. So if you want to build it then first you have to setup kubectl in machine and authorise google account. I could have used gitlab CI/CD pipelines to automate deployment. but due to timing constrain I could not. 

* I was able to figure our how to write gitlab-ci.yaml file, but I was facing issue in building docker images in gitlab runner. To do this we have three ways. But main challange for me to setup new machine and install gitlab runner and register with gitlab. I know the process but due to timing constrain I could not.
* To verify Contract Testing, I am using real database on backend side. We can also use mock of DB.
* But for unit testing I am using mock db.

